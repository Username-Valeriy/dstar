import Vue from 'vue'
import App from './App.vue'
import './registerServiceWorker'
import store from './store'
import OverlayScrollbars from 'overlayscrollbars'
import Header from './components/view/Header'
import TopBanner from './components/index_component/TopBanner'
import HowItWorks from './components/index_component/HowItWorks'
import MainFeatures from './components/index_component/MainFeatures'
import FreeSupport from './components/index_component/FreeSupport'


Vue.config.productionTip = false

Vue.component('Header', Header);
Vue.component('TopBanner', TopBanner);
Vue.component('HowItWorks', HowItWorks);
Vue.component('MainFeatures', MainFeatures);
Vue.component('FreeSupport', FreeSupport);

import "overlayscrollbars/css/OverlayScrollbars.min.css";
import '@/assets/scss/style.scss';

new Vue({
  store,
  render: h => h(App)
}).$mount('#app')


document.addEventListener("DOMContentLoaded", function() {
  OverlayScrollbars(document.querySelectorAll('body'), { });

});
